#!/bin/sh

set -e

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing required packages"
PKG_LIB="openjdk8-jre ttf-dejavu ffmpeg flac lame"
PKG_DEV=""
docker-install -r -u ${PKG_LIB} ${PKG_DEV}
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Setting build environment"
AIRSONIC_VERSION="$(wget --no-check-certificate -q -O - https://api.github.com/repos/airsonic/airsonic/releases | grep '^    "name":' | head -1 | sed -e 's/^.*"name": "Airsonic \(.*\)",$/\1/g')"
AIRSONIC_URL="https://github.com/airsonic/airsonic/releases/download/v${AIRSONIC_VERSION}/airsonic.war"
echo "AIRSONIC_VERSION : ${AIRSONIC_VERSION}"
echo "AIRSONIC_URL     : ${AIRSONIC_URL}"
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing airsonic"
mkdir -p /app/airsonic
cd /app/airsonic
wget --no-check-certificate -q "${AIRSONIC_URL}"
cat << EOF > /etc/profile.d/airsonic.sh
export AIRSONIC_HOME=/app/airsonic
EOF
chmod 755 /etc/profile.d/airsonic.sh
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Uninstalling unnecessary packages"
docker-install -d -c ${PKG_DEV}
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Setting basic configurations"
mkdir -p /conf.d /conf.d/airsonic /conf.d/airsonic/transcode
cd /conf.d/airsonic/transcode
for transcoder in ffmpeg flac lame; do ln -s "$(which $transcoder)"; done
cd /
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

