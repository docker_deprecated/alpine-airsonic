#!/bin/sh

. /etc/profile

LANG="${LANG:-ko_KR.UTF-8}"
SUBSONIC_HOME="${SUBSONIC_HOME:-/app/airsonic}"

SERVER_PORT="${SERVER_PORT:-4040}"
SERVER_ADDRESS="${SERVER_ADDRESS:-0.0.0.0}"

exec java -Dserver.port=${SERVER_PORT} -Dserver.address=${SERVER_ADDRESS} -Dairsonic.home="/conf.d/airsonic" -Xmx512m -Djava.awt.headless=true -jar ${SUBSONIC_HOME}/airsonic.war

