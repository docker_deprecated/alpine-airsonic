# alpine-airsonic

#### [alpine-x64-airsonic](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-airsonic/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64/alpine-x64-airsonic/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64/alpine-x64-airsonic/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64/alpine-x64-airsonic/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-airsonic)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-airsonic)
#### [alpine-aarch64-airsonic](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-airsonic/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64/alpine-aarch64-airsonic/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64/alpine-aarch64-airsonic/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64/alpine-aarch64-airsonic/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-airsonic)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-airsonic)
#### [alpine-armhf-airsonic](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-airsonic/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinearmhf/alpine-armhf-airsonic/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinearmhf/alpine-armhf-airsonic/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinearmhf/alpine-armhf-airsonic/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-airsonic)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-airsonic)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [Airsonic](https://airsonic.github.io/)
    - Airsonic is a free, web-based media streamer, providing ubiquitous access to your music.



----------------------------------------
#### Run

```sh
docker run -d \
           -p 4040:4040/tcp \
           -v /conf.d:/conf.d \
           forumi0721alpine[ARCH]/alpine-[ARCH]-airsonic:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:4040/](http://localhost:4040/)
    - Default username/password : admin/admin



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 4040/tcp           | Serivce port                                     |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

